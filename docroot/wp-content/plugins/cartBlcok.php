<?php
/*
Plugin Name: ConfigBox Cart Info
Plugin URI: http://www.configbox.at/
Description: Plugin for the show ConfigBox cart information.
Version: 1.0
Author: Rovexo
Author URI: http://www.configbox.at/
License: GPL2
*/


// The widget class
class Cb_Cart_Widget extends WP_Widget {

	// Main constructor
	public function __construct() {
		parent::__construct(
		// Base ID of your widget
			'wpb_widget',

			// Widget name will appear in UI
			__('CB Cart Info', 'Widget_for_configbox'),

			// Widget description
			array( 'description' => __( 'Widget for the show Configbox cart information' ), )
		);

	}

	// The widget form (for the backend )
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'widget_admin_view' );
		}
		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="cart_block" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php
	}

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		$view = KenedoView::getView('ConfigboxViewBlockCart');
		$cartView = $view->getHtml();

		// This is where you run the code and display the output
		echo __($cartView, 'show_cart_view');
		echo $args['after_widget'];
	}

}


// Register the widget
function register_cart_block_widget() {
	register_widget( 'Cb_Cart_Widget' );
}
add_action( 'widgets_init', 'register_cart_block_widget' );
