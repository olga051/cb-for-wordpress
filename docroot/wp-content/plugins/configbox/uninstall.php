<?php
defined('ABSPATH') or die('No direct access');

if (!defined( 'WP_UNINSTALL_PLUGIN')) {
	exit;
}

// Init Kenedo framework
require_once(__DIR__ . '/app/external/kenedo/helpers/init.php');
initKenedo('com_configbox');

// We delete any ConfigBox cache data
ConfigboxCacheHelper::purgeCache();

$db = KenedoPlatform::getDb();

// Get all configbox and cbcheckout tables
$query = "
	SELECT `TABLE_NAME`
	FROM `information_schema`.`TABLES` 
	WHERE `TABLE_SCHEMA` = DATABASE() AND (`TABLE_NAME` LIKE '#__configbox_%' OR `TABLE_NAME` LIKE '#__cbcheckout_%')
	";
$db->setQuery($query);
$tables = $db->loadResultList();

if (count($tables)) {

	// Quote the tables for the FK constraint query below
	$quotedTables = array();
	foreach ($tables as $table) {
		$quotedTables[] = "'".$table."'";
	}

	// Now we get info on all foreign key constraints that reference cb tables
	$query = "
		SELECT `TABLE_NAME`, `COLUMN_NAME`, `CONSTRAINT_NAME`, `REFERENCED_TABLE_NAME`, `REFERENCED_COLUMN_NAME`
		FROM `INFORMATION_SCHEMA`.`KEY_COLUMN_USAGE`
		WHERE `REFERENCED_TABLE_SCHEMA` = DATABASE() AND `REFERENCED_TABLE_NAME` IN (".implode(',', $quotedTables).")";
	$db->setQuery($query);
	$infos = $db->loadAssocList();

	// And we remove them (so deleting the tables is easy)
	foreach ($infos as $info) {
		$query = "ALTER TABLE `".$info['TABLE_NAME']."` DROP FOREIGN KEY `".$info['CONSTRAINT_NAME']."`";
		$db->setQuery($query);
		$db->query();
	}

	// Finally, we drop the tables themselves
	foreach ($tables as $table) {
		$query = "DROP TABLE IF EXISTS `".$table."`";
		$db->setQuery($query);
		$db->query();
	}

}

// This is for KSession - it tells the shutdown function not to deal with the (now dropped) session table
define('CONFIGBOX_GOT_UNINSTALLED', 1);
