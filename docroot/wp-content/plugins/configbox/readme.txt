=== configbox ===
Contributors: Rovexo
Tags: ecommerce, configurator
Requires at least: 4.0.1
Tested up to: 4.9.x
Stable tag: 3.1.1
License: Proprietary
License URI: https://www.configbox.at/external/documents/ConfigBox_EULA_EN.pdf

Configbox is a web-based product configurator and ecommerce application.

== Description ==

See https://www.configbox.at