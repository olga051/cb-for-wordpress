<?php
jimport('joomla.html.parameter.element');

/** @noinspection PhpDeprecationInspection */

/** @noinspection PhpUndefinedClassInspection */
class JElementListinglegacy extends JElement {
	
	public $_name = 'Listinglegacy';

	function fetchElement($name, $value, &$node, $control_name) {
		
		// Init Kenedo framework
		require_once( dirname(__FILE__).'/../../../init.php');
		
		$pickerObject = $name;
		$fieldName = $control_name.'['.$pickerObject.']';
		
		$link = KLink::getRoute('index.php?option=com_configbox&controller=adminlistings&tmpl=component&parampicker=1&pickerobject='.$pickerObject);

		KenedoViewHelper::loadKenedoAssets();

		$db = KenedoPlatform::getDb();
		$tag = KenedoPlatform::p()->getLanguageTag();
		$query = "SELECT * FROM `#__configbox_active_languages` WHERE `tag` = '".$db->getEscaped($tag)."'";
		$db->setQuery($query);
		$isActive = (boolean)$db->loadResult();
		if (!$isActive) {
			$query = "SELECT `language_tag` FROM `#__configbox_config` WHERE `id` = 1";
			$db->setQuery($query);
			$tag = $db->loadResult();
		}
		
		$item = new stdClass;
		
		if ($value) {
			$item->title = ConfigboxCacheHelper::getTranslation('#__configbox_strings', 20, $value, $tag);
		} 
		else {
			$item->title = KText::_('All');
		}
		
		ob_start();
		?>
		<div style="float: left;">
			<input style="background: #ffffff;" type="text" id="<?php echo $pickerObject;?>_name" value="<?php echo hsc($item->title);?>" disabled="disabled" />
		</div>
		<div class="button2-left">
			<div class="blank"><a class="trigger-picker-select" href="<?php echo $link;?>"><?php echo KText::_('Select');?></a></div>
			<div class="blank"><a class="trigger-picker-reset" onclick="cbj('#<?php echo $pickerObject;?>_id').val('0'); cbj('#<?php echo $pickerObject;?>_name').val('<?php echo KText::_('All');?>');"><?php echo KText::_('Reset');?></a></div>
			<input type="hidden" id="<?php echo $pickerObject;?>_id" name="<?php echo $fieldName;?>" value="<?php echo (int)$value;?>" />
		</div>
		<?php
		$html = ob_get_clean();
		
		return $html;
	}
}
