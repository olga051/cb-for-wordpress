<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewProductlisting */
header('Gone', true, 410);
?>
<div <?php echo $this->getViewAttributes();?>>
	<div class="notice-listing-not-found">
		<?php echo KText::_('Product listing not found.');?>
	</div>
</div>
