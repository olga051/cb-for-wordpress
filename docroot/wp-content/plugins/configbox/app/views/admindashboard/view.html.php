<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewAdmindashboard extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = 'admindashboard';

	/**
	 * @var object[] List of issues that absolutely need to be resolved
	 * @see ConfigboxModelAdmindashboard::getCriticalIssues
	 */
	public $criticalIssues;

	/**
	 * @var object[] List of issues that may cause problems
	 * @see ConfigboxModelAdmindashboard::getIssues
	 */
	public $issues;

	/**
	 * @var object[] List of performance tips
	 * @see ConfigboxModelAdmindashboard::getPerformanceTips
	 */
	public $performanceTips;

	/**
	 * @var object[] List of server stats
	 * @see ConfigboxModelAdmindashboard::getCurrentStats
	 */
	public $currentStats;

	/**
	 * @var bool Indicates if product tour should be shown
	 */
	public $showProductTour = false;

	/**
	 * @var string HTML for the admin product tour
	 * @see ConfigboxViewAdminproducttour
	 */
	public $tourHtml;

	/**
	 * @return ConfigboxModelAdmindashboard
	 * @throws Exception
	 */
	function getDefaultModel() {
		return KenedoModel::getModel('ConfigboxModelAdmindashboard');
	}

	function getJsInitCallsEach() {
		return array(
			'configbox/dashboard::initDashboard'
		);
	}

	function prepareTemplateVars() {

		if (ConfigboxSystemVars::getVar('post_install_done') == NULL) {
			KenedoPlatform::p()->redirect(KLink::getRoute('index.php?option=com_configbox&controller=adminpostinstall'));
		}

		if (ConfigboxSystemVars::getVar('admin_tour_done') == NULL) {
			$tourView = KenedoView::getView('ConfigboxViewAdminproducttour');
			$this->showProductTour = false;
			$this->tourHtml = $tourView->getHtml();
		}

		$model = $this->getDefaultModel();

		$this->criticalIssues = $model->getCriticalIssues();
		$this->issues = $model->getIssues();
		$this->performanceTips = $model->getPerformanceTips();
		$this->currentStats = $model->getCurrentStats();

	}
	
}
