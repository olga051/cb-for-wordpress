/**
 * @module configbox/dashboard
 */
define(['cbj', 'configbox/server'], function(cbj, server) {
	"use strict";
	/**
	 * @exports configbox/dashboard
	 */
	var module = {

		initDashboard: function() {

			// Update news and software update check
			module.getUpdate();

			cbj(document).on('click', '.view-admindashboard .trigger-remove-file-structure-warning', function() {

				server.makeRequest('admindashboard', 'removeFileStructureWarning');

				cbj(this).closest('.issue-item').remove();

			});

		},

		getUpdate : function() {

			var data = module.getDataFromCache();

			if (data) {
				module.insertData(data);
				return;
			}

			var parameters = {
				'platform': server.config.platformName,
				'lang': server.config.languageCode,
				'version': server.config.cbVersion
			};

			cbj.ajax({
				url: 'https://www.configbox.at/external/dashboard/dashboard.php',
				dataType: 'jsonp',
				crossDomain: true,
				data: parameters,
				success: function(data){
					module.cacheData(data);
					module.insertData(data);
				},
				error: function() {
					cbj('.view-admindashboard .news').html('Cannot load news at this time.');
				}

			});

		},

		cacheData: function(data) {
			if (typeof(window.sessionStorage) != 'undefined') {
				window.sessionStorage.updateInfo = JSON.stringify(data);
			}
		},

		getDataFromCache: function() {
			if (typeof(sessionStorage) != 'undefined' && typeof(window.sessionStorage.updateInfo) != 'undefined') {
				return JSON.parse(window.sessionStorage.updateInfo);
			}
		},

		/**
		 * Inserts json-derived data into the dashboard
		 * @param {JsonResponses.dashboardData} data
		 */
		insertData: function(data) {
			cbj('.view-admindashboard .news .news-target').html(data.news);
			cbj('.view-admindashboard .news').css('visibility','visible');

			cbj('.checking-for-update').hide();

			if (data.softwareUpdate.url) {
				cbj('.software-update-link').attr('href',data.softwareUpdate.url);
			}

			if (data.softwareUpdate.patchLevel) {
				cbj('.latest-version-patchlevel').text(data.softwareUpdate.patchLevel);
				cbj('.patchlevel-update-available').show();
			}

			if (data.softwareUpdate.major) {
				cbj('.latest-version-major').text(data.softwareUpdate.major);
				cbj('.major-update-available').show();
			}

			if (!data.softwareUpdate.major && !data.softwareUpdate.patchLevel) {
				cbj('.no-update-available').show();
			}
		}

	};

	return module;

});
