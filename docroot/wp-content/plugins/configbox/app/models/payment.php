<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxModelPayment extends KenedoModel {

	/**
	 * @param ConfigboxUserData $orderAddress
	 * @param float $baseDeliveryAndOrder base gross price of shipping + order
	 * @return ConfigboxPaymentmethodData[] possible payment options
	 */
	function getPaymentOptions($orderAddress = NULL, $baseDeliveryAndOrder = NULL) {
		
		if (!$orderAddress) {
			$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
			$orderId = $orderModel->getId();
			$order = $orderModel->getOrderRecord($orderId);
			$orderAddress = $order->orderAddress;
		}
		
		if ($baseDeliveryAndOrder === NULL) {
			$orderModel = KenedoModel::getModel('ConfigboxModelOrderrecord');
			$orderRecordId = $orderModel->getId();
			$order = $orderModel->getOrderRecord($orderRecordId);
			
			if ($order->isVatFree) {
				$baseDeliveryAndOrder = $order->baseTotalNet + $order->delivery->basePriceNet;
			}
			else {
				$baseDeliveryAndOrder = $order->baseTotalGross + $order->delivery->basePriceGross;
			}
			
		}
		
		$ass = ConfigboxCacheHelper::getPaymentMethodAssignments();
		
		$paymentIds = isset($ass['country_to_payment'][intval($orderAddress->billingcountry)]) ? $ass['country_to_payment'][intval($orderAddress->billingcountry)] : array();
		$payments = array();
		foreach ($paymentIds as &$paymentId) {	
			$payments[$paymentId] = $this->getPaymentOption($paymentId, $baseDeliveryAndOrder, $orderAddress);
			$payments[$paymentId]->sortHelper = number_format($payments[$paymentId]->basePriceGross,3).$payments[$paymentId]->ordering;
		}
		
		// Sort by price
		$sortFunction = create_function('$a,$b', 'return $a->sortHelper > $b->sortHelper;');
		
		usort($payments,$sortFunction);
		
		return $payments;
		
	}

	/**
	 * @param int $paymentId payment method id
	 * @param float $baseDeliveryAndOrder base gross price of shipping + order
	 * @param ConfigboxUserData $orderAddress
	 * @return ConfigboxPaymentmethodData payment option
	 */
	function getPaymentOption($paymentId, $baseDeliveryAndOrder, $orderAddress) {
		
		$baseDeliveryAndOrder = (float)$baseDeliveryAndOrder;
		
		$paymentOptions = ConfigboxCacheHelper::getFromCache('paymentOptions');
		if ($paymentOptions === NULL) {
			$db = KenedoPlatform::getDb();
			$query = "SELECT * FROM `#__configbox_payment_methods`";
			$db->setQuery($query);
			$paymentOptions = $db->loadObjectList('id');
			ConfigboxCacheHelper::writeToCache('paymentOptions', $paymentOptions);
			$paymentOptions = ConfigboxCacheHelper::getFromCache('paymentOptions');
		}
		
		if (!isset($paymentOptions[$paymentId])) {
			return null;
		}
		
		$payment = $paymentOptions[$paymentId];
		
		$payment->title 		= ConfigboxCacheHelper::getTranslation('#__configbox_strings', 46, $paymentId);
		$payment->description 	= ConfigboxCacheHelper::getTranslation('#__configbox_strings', 47, $paymentId);
		
		$payment->settings = new KStorage($payment->params);
		$price = $payment->price;
		unset($payment->price);
		
		// Add percentage to it
		$payment->basePriceNet = round($price + $baseDeliveryAndOrder * $payment->percentage / 100, 4);
		
		// Cap by min and max price
		if ($payment->price_min != 0 && $price < $payment->price_min) {
			$payment->basePriceNet = $payment->price_min;
		}
		if ($payment->price_max != 0 && $price > $payment->price_max) {
			$payment->basePriceNet = $payment->price_max;
		}
		
		$taxRate = ConfigboxUserHelper::getTaxRate($payment->taxclass_id, $orderAddress);
		
		$payment->taxRate = $taxRate;

		$payment->basePriceTax = $payment->basePriceNet * $taxRate / 100;
		$payment->basePriceGross = $payment->basePriceNet + $payment->basePriceTax;
		
		ConfigboxCurrencyHelper::appendCurrencyPrices($payment);
		
		return $payment;
		
	}
	
}