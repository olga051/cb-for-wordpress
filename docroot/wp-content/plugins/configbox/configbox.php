<?php
/*
Plugin Name: ConfigBox
Plugin URI: https://www.configbox.at
Description: Configbox is a web-based product configurator and ecommerce application.
Version: 3.1.1
Author: Rovexo
Author URI: https://www.configbox.at
License: Proprietary
Text Domain: configbox
*/

defined('ABSPATH') or die('No direct access');


register_activation_hook(__FILE__, 'activate_configbox' );
register_deactivation_hook(__FILE__, 'deactivate_configbox' );
register_uninstall_hook(__FILE__, 'uninstall_configbox');

add_action('init', 'configbox_init_kenedo' );
add_action('init', 'configbox_add_shortcodes' );
add_action('init', 'configbox_register_custom_post_type');

add_filter('template_include', 'cb_use_loader_template_on_configbox', 0, 1);

add_action( 'init', 'configbox_allow_requested_page' );
add_action( 'admin_init', 'configbox_allow_requested_page' );
add_action( 'admin_menu', 'configbox_add_admin_menu_items', 9 );
add_action( 'admin_menu', 'configbox_allow_requested_page', 9 );

add_filter( 'post_type_link', 'cb_remove_slug', 10, 3 );
add_action( 'pre_get_posts', 'cb_parse_request' );


function cb_use_loader_template_on_configbox($template) {
	if (!empty($_REQUEST['option']) && $_REQUEST['option'] == 'com_configbox') {
		return __DIR__.'/templates/general.php';
	}
	else {
		return $template;
	}
}

function cb_remove_slug( $post_link, $post, $leavename ) {

	$types = array_keys(configbox_get_custom_post_types());

	if (!in_array($post->post_type, $types)) {
		return $post_link;
	}

	if ('publish' != $post->post_status) {
		return $post_link;
	}

	$post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );

	return $post_link;
}


/**
 * @param WP_Query $query
 */
function cb_parse_request( $query ) {

	if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
		return;
	}

	$types = array_keys(configbox_get_custom_post_types());

	if ( ! empty( $query->query['name'] ) ) {
		$postTypeSearch = array_merge($types, ['post', 'page']);
		$query->set( 'post_type', $postTypeSearch );
	}
}

function configbox_get_custom_post_types() {

	return [

		'cb_product_listing' =>
			[
				'labels'      => [
					'name'          => 'Product Listings',
					'singular_name' => 'Product Listing',
				],
				'public'      => false,
				'has_archive' => false,
				'rewrite'     => ['slug' => 'cb_product_listing'],
				'supports' => array( 'title', 'custom-fields' )
			],

		'cb_product' =>
			[
				'labels'      => [
					'name'          =>'Products',
					'singular_name' => 'Product',
				],
				'public'      => false,
				'has_archive' => false,
				'rewrite'     => ['slug' => 'cb_product'],
				'supports' => array( 'title', 'custom-fields' ),

			],

		'cb_page' =>
			[
				'labels'      => [
					'name'          => 'Pages',
					'singular_name' => 'Page',
				],
				'public'      => false,
				'has_archive' => false,
				'rewrite'     => ['slug' => 'cb_page'],
				'supports' => array( 'title', 'custom-fields' ),

			],

		'cb_internal' =>
			[
				'labels'      => [
					'name'          => 'Internal Pages',
					'singular_name' => 'Internal Page',
				],
				'public'      => false,
				'has_archive' => false,
				'rewrite'     => ['slug' => 'cb_internal'],
				'supports' => array( 'title', 'custom-fields' ),

			],

	];

}

function configbox_register_custom_post_type() {

	$types = configbox_get_custom_post_types();

	$showInMenu = true;

	foreach ($types as $name => $args) {

		if ($showInMenu) {
			$args['show_in_menu'] = true;
			$args['show_in_admin_bar'] = true;
			$args['show_in_nav_menus'] = true;
		}

		register_post_type($name, $args);

	}

}

function configbox_add_shortcodes() {
	add_shortcode( 'configbox', 'configbox_main_shortcode_listener');
}

function configbox_main_shortcode_listener($attributes) {

	configbox_init_kenedo();

	$view = $attributes['view'];

	if ($view == 'productlisting') {
		$view = KenedoView::getView('ConfigboxViewProductlisting');
		$view->setListingId($attributes['id']);
		return $view->getHtml();
	}

	if ($view == 'product') {
		$view = KenedoView::getView('ConfigboxViewProduct');
		$view->setProductId($attributes['id']);
		return $view->getHtml();
	}

	if ($view == 'configuratorpage') {
		$view = KenedoView::getView('ConfigboxViewConfiguratorpage');
		$view->setPageId($attributes['id']);
		return $view->getHtml();
	}

	if ($view == 'cart') {
		$view = KenedoView::getView('ConfigboxViewCart');
		return $view->getHtml();
	}

	if ($view == 'checkout') {
		$view = KenedoView::getView('ConfigboxViewCheckout');
		return $view->getHtml();
	}

	if ($view == 'user') {
		$view = KenedoView::getView('ConfigboxViewUser');
		return $view->getHtml();
	}

	return 'Unknown shortcode';

}

function activate_configbox() {
	// Checks for ionCube Loader and deactivates the plugin if there is a problem
	configbox_deactivate_on_loader_issue();
}

function deactivate_configbox() {

}

function uninstall_configbox() {

}

function configbox_deactivate_on_loader_issue() {

	$minLoaderVersion = '10.2';

	$loaderInstalled = extension_loaded('ionCube Loader');

	$versionOk = true;
	if (function_exists('ioncube_loader_version')) {
		$installedVersion = ioncube_loader_version();
		if (version_compare($installedVersion, $minLoaderVersion) == -1) {
			$versionOk = false;
		}
	}

	if ($loaderInstalled == false || $versionOk == false) {
		deactivate_plugins(plugin_basename(__FILE__));
		wp_die('You need to install ionCube Loader 10.2 or higher to run ConfigBox. Please refer to the manual on how to install it.');
	}

}

function configbox_init_kenedo() {
	require_once(__DIR__ . '/app/external/kenedo/helpers/init.php');
	initKenedo('com_configbox');
	// Just to force start a session early
	KSession::get('dummy');
}

function configbox_add_admin_menu_items() {
	configbox_init_kenedo();
	add_menu_page(KText::_('ConfigBox'), KText::_('ConfigBox'), 'edit_pages', 'admindashboard', 'configbox_execute_controller_task', '', 25);
}

function configbox_allow_requested_page() {

	// We best don't manipulate if the request does not deal with a CB page. For now let it be existence of controller or view param
	if (empty($_REQUEST['controller']) and empty($_REQUEST['view'])) {
		return;
	}

	configbox_init_kenedo();

	$page = KRequest::getKeyword('page', '');
	$task = KRequest::getKeyword('task', '');

	// Action can override page and task. Convention is that action consists of page name, then a dot, then task name
	$action = KRequest::getKeyword('action', '');
	if (count(explode('.', $action)) == 2) {
		$ex = explode('.', $action);
		$page = $ex[0];
		$task = $ex[1];
	}

	// Set the ajax action name
	$action = 'wp_ajax_' . $page . '.' . $task;
	add_action($action, 'configbox_execute_controller_task');

	// Add another action for requests that need clean CB only output. This avoids the 0 that WP adds to admin-ajax call responses.
	$format = KRequest::getKeyword('format', '');
	if (in_array($format, array('raw', 'json'))) {
		add_action($action, 'configbox_prevent_the_freakin_zero_in_output');
	}

	// On init, that function isn't available
	if (function_exists('add_submenu_page')) {
		// Add a pseudo submenu page just to allow the page request
		add_submenu_page(null, '', '', 'edit_pages', $page, 'configbox_execute_controller_task');
	}

}

function configbox_prevent_the_freakin_zero_in_output() {
	wp_die();
}

function configbox_execute_controller_task() {

	configbox_init_kenedo();

	// Set the option request var like for Joomla, as all is built around that
	if (empty($_REQUEST['option'])) {
		$_REQUEST['option'] = 'com_configbox';
	}

	$page = KRequest::getKeyword('page','');
	$task = KRequest::getKeyword('task','display');

	// Action can override page and task. Convention is that action consists of page name, then a dot, then task name
	$action = KRequest::getKeyword('action','');
	if (count(explode('.', $action)) == 2) {
		$ex = explode('.', $action);
		$page = $ex[0];
		$task = $ex[1];
	}

	KLog::log('Got execution request for page "'.$page.'", task "'.$task.'"', 'custom_wp_requests');

	$className = KenedoController::getControllerClass('com_configbox', $page);

	if (KenedoController::controllerExists($className) == false) {

		KLog::log('User requested page "'.$page.'", but no controller found for that page.', 'custom_wp_routing');
		do_action( 'admin_page_access_denied' );

		wp_die( __( 'Sorry, you are not allowed to access this page.' ), 403 );
	}

	$controller = KenedoController::getController($className);

	// Start a new output buffer
	ob_start();

	try {
		$controller->execute($task);
	}
	catch (Exception $e) {
		KLog::log('Error occurred during executing controller task "'.$className.':'.$task.'. File "'.$e->getFile().':'.$e->getLine().'". Exception text was '.$e->getMessage().'. Trace: '. "\n".$e->getTraceAsString(), 'error');
		echo 'An error occurred. See CB log file';
	}

	// Get the output so far in a variable
	$output = ob_get_clean();

	// Redirect if a task handler has set a redirectUrl
	if ($controller->redirectUrl) {
		$controller->redirect();
	}
	else {

		// Send output through observers
		KenedoObserver::triggerEvent('onBeforeRender', array(&$output));
		KenedoPlatform::p()->renderOutput($output);

		// Restore error handler to give the platform a normal environment
		KenedoPlatform::p()->restoreErrorHandler();

	}

}
