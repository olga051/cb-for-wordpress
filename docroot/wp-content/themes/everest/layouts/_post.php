<article id="item-<?php the_ID(); ?>" class="uk-article tm-article-date-true" data-permalink="<?php the_permalink(); ?>">

    <?php if (has_post_thumbnail()) : ?>
        <?php
        $width = get_option('thumbnail_size_w'); //get the width of the thumbnail setting
        $height = get_option('thumbnail_size_h'); //get the height of the thumbnail setting
        ?>
        <div class="tm-article-featured-image">
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(array($width, $height), array('class' => '')); ?></a>
        </div>
    <?php endif; ?>

    <div class="tm-article-wrapper">

        <div class="tm-article-date uk-hidden-small">
            <?php printf('<span class="tm-article-date-day">'.get_the_date('d M').'</span>'.'<span class="tm-article-date-year">'.get_the_date('Y').'</span>'); ?>
        </div>

        <h1 class="uk-article-title"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

        <p class="uk-article-meta">
        <?php
            $date = '<time datetime="'.get_the_date('Y-m-d').'" pubdate>'.get_the_date().'</time>';
            printf(__('%s Written by %s. Posted in %s', 'warp'), '<span class="uk-visible-small">'.$date.'. </span>', '<a href="'.get_author_posts_url(get_the_author_meta('ID')).'" title="'.get_the_author().'">'.get_the_author().'</a>', get_the_category_list(', '));
        ?>
        </p>

        <div class="tm-article-content">
            <?php the_content(''); ?>
        </div>

        <p>
            <a class="uk-button" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php _e('Continue Reading', 'warp'); ?></a>
            <?php if (comments_open() || get_comments_number()) comments_popup_link(__('No Comments', 'warp'), __('1 Comment', 'warp'), __('% Comments', 'warp'), "uk-button", ""); ?>
        </p>

        <?php edit_post_link(__('Edit this post.', 'warp'), '<p><i class="uk-icon-pencil"></i> ','</p>'); ?>

    </div>

</article>
