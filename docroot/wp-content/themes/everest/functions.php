<?php
/**
* @package   Everest
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// check compatibility
if (version_compare(PHP_VERSION, '5.3', '>=')) {

    // bootstrap warp
    require(__DIR__.'/warp.php');
}


function redirect_to_home() {
	if(is_user_logged_in() && is_page('registration')) {
		wp_redirect(home_url());
		exit();
	}
 
  if(!is_user_logged_in() && stripos($_SERVER['REQUEST_URI'], '/account') !== false){
    wp_redirect(home_url());
		exit();
  }
}
add_action('template_redirect', 'redirect_to_home');
