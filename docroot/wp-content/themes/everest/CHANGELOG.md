# Changelog

	1.0.11
	^ Updated UIkit theme according to UIkit 2.20.0
	# Fixed Article Navigation (J)

	1.0.10
	+ Added WooCommerce products per page option (WP)

    1.0.9
    ^ Updated UIkit theme according to UIkit 2.17.0

    1.0.8
    ^ Updated UIkit theme according to UIkit 2.15.0

    1.0.7
    ^ Updated UIkit theme according to UIkit 2.1.1

    1.0.6
    # Added missing navbar fixed option (J25)

    1.0.5
    ^ Updated theme according to UIkit 2.10.0
    + Added WooCommerce Layer

    1.0.4
    ^ Updated theme according to UIkit 2.8.0

    1.0.3
    # Added missing sticky navbar option (WP)
    ^ Updated sticky navbar background color

    1.0.2
    ^ Added sticky navbar option
    ^ Removed content-boxed option
    ^ Updated category override (J)
    # Fixed offcanvas search padding

    1.0.1
    # Remove headerbar position
    ^ Prevent search-field uk-active issue
    + Added toolbar-c position

    1.0.0
    + Initial Release



    * -> Security Fix
    # -> Bug Fix
    $ -> Language fix or change
    + -> Addition
    ^ -> Change
    - -> Removed
    ! -> Note
